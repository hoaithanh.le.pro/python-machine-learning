import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import hvplot.pandas  # noqa

pd.options.plotting.backend = 'holoviews'

sns.set()
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

os.chdir('Mastery Execises')
df = pd.read_csv('./Dataset/USA_Housing.csv')

df.head()
df.info()
df.describe()
sns.palplot(df)
plt.show()

df.hvplot.hist(by='Price', subplots=False, width=1000)